<%@page import="java.util.Enumeration"%>
<%@page import="java.util.List"%>
<%@page import="com.Otium.Shop.Entity.ProductParamType"%>
<%@page import="com.Otium.Shop.Controls.ControlProduct"%>
<%@page import="com.Otium.Shop.Entity.ProductParam"%>
<%@page import="com.Otium.Shop.Controls.ControlProductParamType"%>
<%@page import="com.Otium.Shop.Entity.Product"%>
<%@page import="com.Otium.Shop.Entity.ProductType"%>
<%@page import="com.Otium.Shop.Controls.ControlProductType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
    request.setCharacterEncoding( "UTF-8" );
	ProductType type = ControlProductType.getTypesList().get(0);
	if(request.getParameter("ProductType")!=null)
		type = ControlProductType.getType(Integer.parseInt(request.getParameter("ProductType")));
	if(request.getParameter("Update")!=null){
		Enumeration<String> names;
		if(request.getParameter("ProductName")!=null && !request.getParameter("ProductName").equals("")){
			Product product = new Product(request.getParameter("ProductName"), type);
			if(!request.getParameter("ProductPrice").equals(""))
				product.setPrice(Double.parseDouble(request.getParameter("ProductPrice")));
			if(!request.getParameter("ProductRest").equals(""))
				product.getRest().setQuantity(Double.parseDouble(request.getParameter("ProductRest")));
			ControlProduct.addProduct(product);
			for(ProductParam param : ControlProduct.getProductParam(product)){
				param.setValue(request.getParameter("ProductParamType"+param.getProductParam().getRid()));
				ControlProduct.updateProductParam(param);
			}
		}
		
		names = request.getParameterNames();
		while(names.hasMoreElements()) {
			String string = names.nextElement();
			if(string.startsWith("ProductName_")){
				String id = string.substring(12);				
				Product product = ControlProduct.getProduct(Integer.parseInt(id));
				if(product!=null){
					out.print("found " + product + "<br>");
					product.setName(request.getParameter("ProductName_" + id));
					product.setPrice(Double.parseDouble(request.getParameter("ProductPrice_" + id)));
					product.getRest().setQuantity(Double.parseDouble(request.getParameter("ProductRest_" + id)));
					ControlProduct.updateProduct(product);
					out.print("updated " + product + "<br>");
					
					for(ProductParam param : ControlProduct.getProductParam(product)){
						if(param.getRid()==0)
							param.setValue(request.getParameter("ProductParamType"+param.getProductParam().getRid()+"_"+id));
						else
							param.setValue(request.getParameter("ProductParam"+param.getRid()+"_"+id));
						ControlProduct.updateProductParam(param);
					}
					
				}				
			}
		}
		
		response.sendRedirect("production.jsp?ProductType="+type.getRid());
	}
	
	if(request.getParameter("Remove")!=null){
		ControlProduct.removeProduct(Integer.parseInt(request.getParameter("Product")));
		response.sendRedirect("production.jsp?ProductType="+type.getRid());
	}
	
    List<ProductParamType> param_type_list = ControlProductType.getProductParamTypes(type);
%>
<div>
	<form action="production.jsp?Update=yes&ProductType=<% out.print(type.getRid()); %>" method = "POST" >
		<table>
		<tr>
			<th>Наименование</th>
			<th>Цена</th>
			<th>Остаток</th>
			<% for(ProductParamType param_type : param_type_list) { %>
				  <th><% out.println(param_type.getName()); %></td>   	
			<% } %>	
		</tr>	
		<% for(Product product : type.getProductList()){%>
			<tr>
				<td><input type="text" name="ProductName_<% out.print(product.getRid()); %>" value="<% out.println(product.getName()); %>"><br></td>
				<td><input type="text" name="ProductPrice_<% out.print(product.getRid()); %>" value="<% out.println(product.getPrice()); %>"><br></td>
				<td><input type="text" name="ProductRest_<% out.print(product.getRid()); %>" value="<% out.println(product.getRest().getQuantity()); %>"><br></td>
				<% for(ProductParam param : ControlProduct.getProductParam(product)) {
				   		if(param.getRid()==0){ %>
				   		<td><input type="text" name="ProductParamType<% out.print(param.getProductParam().getRid()+"_"+product.getRid()); %>" value="<% out.println(param.getValue()!=null?param.getValue():""); %>"><br></td>
				   <%	} else { %>
				  		<td><input type="text" name="ProductParam<% out.print(param.getRid()+"_"+product.getRid()); %>" value="<% out.println(param.getValue()!=null?param.getValue():""); %>"><br></td>   	
				   <%   }
				   } %>							
			    <td><a href="production.jsp?Remove=1&ProductType=<% out.print(type.getRid()); %>&Product=<% out.print(product.getRid()); %>">Удалить</a></td>
			</tr>				
		<%}%>
			<tr>
				<td>
					<input type="text" name="ProductName"><br>
				</td>
				<td>
					<input type="text" name="ProductPrice"><br>
				</td>
				<td>
					<input type="text" name="ProductRest"><br>
				</td>
				<% for(ProductParamType param_type : param_type_list) { %>
				  <td><input type="text" name="ProductParamType<% out.print(param_type.getRid()); %>"><br></td>   	
				<% } %>	
			</tr>
			<tr>
				<td>
					<input type="submit" value="Сохранить">
				</td>
			</tr>
		</table>		  
	</form>
</div>