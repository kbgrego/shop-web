<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="com.Otium.Shop.Controls.ControlSpecialPrice"%>
<%@ page import="com.Otium.Shop.Controls.ControlProduct"%>
<%@ page import="com.Otium.Shop.Controls.ControlProductType"%>
<%@ page import="com.Otium.Shop.Controls.ControlProductParamType"%>
<%@ page import="com.Otium.Shop.Controls.ControlStock"%>
<%@ page import="com.Otium.Shop.Entity.ProductType"%>
<%@ page import="com.Otium.Shop.Entity.ProductParamType"%>
<%@ page import="com.Otium.Shop.Entity.ProductTypeParamType"%>
<%@ page import="com.Otium.Shop.Entity.SpecialPrice"%>
<%@ page import="com.Otium.Shop.Entity.Stock"%>
<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
    
<%  
    request.setCharacterEncoding( "UTF-8" );
	if(request.getParameter("Update")!=null){
		Enumeration<String> names;
		switch(Integer.parseInt(request.getParameter("Update"))){
			case 1:
				ControlProductType.addType(request.getParameter("ProductTypeName"));
				names = request.getParameterNames();
				while(names.hasMoreElements()) {
					String string = names.nextElement();
					if(string.startsWith("ProductTypeName_"))
						ControlProductType.updateType(Integer.parseInt(string.substring(16)), 
								                      request.getParameter(string));
				}
				break;
			case 2:
				ControlProductParamType.addType(request.getParameter("ProductParamTypeName"));
				names = request.getParameterNames();
				while(names.hasMoreElements()) {
					String string = names.nextElement();
					if(string.startsWith("ProductParamTypeName_"))
						ControlProductParamType.updateType(Integer.parseInt(string.substring(21)), 
								                           request.getParameter(string));
				}
				break;
			case 3:				
				ControlProductParamType.addType(Integer.parseInt(request.getParameter("ProductParamTypeProductType[]")), 
						                        request.getParameter("ProductTypeParamTypeName"));
				names = request.getParameterNames();
				while(names.hasMoreElements()) {
					String string = names.nextElement();
					if(string.startsWith("ProductTypeParamTypeName_"))
						ControlProductParamType.updateType(Integer.parseInt(string.substring(26)), 
								                           request.getParameter(string));
				}
				break;
			case 4:
				if(request.getParameter("SpecialPriceDiscount")!=null && 
				  !request.getParameter("SpecialPriceDiscount").equals(""))
					ControlSpecialPrice.addSpecialPrice(Integer.parseInt(request.getParameter("SpecialPriceProductParamType")), 
														request.getParameter("SpecialPriceValue"), 
							                            Double.parseDouble(request.getParameter("SpecialPriceDiscount"))); 
				names = request.getParameterNames();
				while(names.hasMoreElements()) {
					String string = names.nextElement();
					if(string.startsWith("SpecialPriceValue_")){
						String id = string.substring(18);
						out.println(id);
						ControlSpecialPrice.updateSpecialPrice(Integer.parseInt(id),
								                               Integer.parseInt(request.getParameter("SpecialPriceProductParamType_" + id)),
								                               request.getParameter("SpecialPriceValue_" + id),		   
								                               Double.parseDouble(request.getParameter("SpecialPriceDiscount_" + id))
						);
					}
				}
				break;
			case 5:
				SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
				if(request.getParameter("StockBegin")!=null &&
				  !request.getParameter("StockBegin").equals("") &&
				   request.getParameter("StockEnd")!=null && 
				  !request.getParameter("StockEnd").equals(""))
					ControlStock.addStock(Integer.parseInt(request.getParameter("StockProductType")), 
										  sdFormat.parse(request.getParameter("StockBegin")), 
										  sdFormat.parse(request.getParameter("StockEnd"))); 
				names = request.getParameterNames();
				while(names.hasMoreElements()) {
					String string = names.nextElement();
					if(string.startsWith("StockProductType_")){
						String id = string.substring(17);
						out.println(id);
						ControlStock.updateStock(Integer.parseInt(id),
								                 Integer.parseInt(request.getParameter("StockProductType_" + id)),
								                 sdFormat.parse(request.getParameter("StockBegin_" + id)),		   
								                 sdFormat.parse(request.getParameter("StockEnd_" + id))
						);
					}
				}
				break;
		}
		response.sendRedirect("settings");
	}

	if(request.getParameter("Remove")!=null){
		switch(Integer.parseInt(request.getParameter("Remove"))){
			case 1:
				ControlProductType.removeType(Integer.parseInt(request.getParameter("ProductType")));
				break;
			case 2:
				ControlProductParamType.removeType(Integer.parseInt(request.getParameter("ProductParamType")));
				break;
			case 3:
				ControlProductParamType.removeType(Integer.parseInt(request.getParameter("ProductTypeParamType")));
				break;
			case 4:
				ControlSpecialPrice.removeSpecialPrice(Integer.parseInt(request.getParameter("SpecialPrice")));
				break;
			case 5:
				ControlStock.removeStock(Integer.parseInt(request.getParameter("Stock")));
				break;
		}
		response.sendRedirect("settings");
	}
		
	if(request.getParameter("RemoveProductParamType")!=null)ControlProductParamType.removeType(Integer.parseInt(request.getParameter("RemoveProductParamType")));
   
%>        
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; UTF-8">
<title>Настройки</title>
<style>
body {
	width: 100%;
}

form {
	padding: 10px;
	text-align: center;
}
</style>
</head>
<body>
<div>
    <a href="shop">Касса</a>
	<a href="warehouse">Склад</a>
	<a href="reports">Отчеты</a>
	<a href="settings">Настройки</a>
</div>
<div style="display:flex; flex-wrap: wrap; ">
	<form action="settings.jsp?Update=1" method = "POST" >
		<b>Типы товаров</b>
		<table>
		<tr>
			<th>Название</th>
			<th></th>
		</tr>	
		<% for(ProductType type : ControlProductType.getTypesList()){%>
			<tr>
				<td><input type="text" name="ProductTypeName_<% out.print(type.getRid()); %>" value="<% out.println(type.getName()); %>"><br></td>							
			    <td><a href="settings.jsp?Remove=1&ProductType=<% out.print(type.getRid()); %>">Удалить</a></td>
			</tr>				
		<%}%>
			<tr>
				<td>
					<input type="text" name="ProductTypeName"><br>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Сохранить">
				</td>
			</tr>
		</table>		  
	</form>
	
	<form action="settings.jsp?Update=2" method = "POST" >
		<b>Общие Характеристики</b>
		<table>
		<tr>
			<th>Название</th>
			<th></th>
		</tr>	
		<% for(ProductParamType type : ControlProductParamType.getTypesList()){%>
			<tr>
				<td><input type="text" name="ProductParamTypeName_<% out.print(type.getRid()); %>" value="<% out.println(type.getName()); %>"><br></td>
			    <td><a href="settings.jsp?Remove=2&ProductParamType=<% out.print(type.getRid()); %>">Удалить</a></td>
			</tr>				
		<%}%>
			<tr>
				<td>
					<input type="text" name="ProductParamTypeName"><br>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Сохранить">
				</td>
			</tr>
		</table>		  
	</form>
	
	<form action="settings.jsp?Update=3" method = "POST" >
		<b>Характеристики по типам товаров</b>
		<table>
		<tr>
			<th>Тип товара</th>
			<th>Название</th>
			<th></th>
		</tr>	
		<% for(ProductTypeParamType type : ControlProductParamType.getTypesProductTypeList()){%>
			<tr>
				<td><% out.println(type.getProductType().getName()); %></td>
				<td><input type="text" name="ProductTypeParamTypeName_<% out.print(type.getRid()); %>" value="<% out.println(type.getName()); %>"><br></td>
			    <td><a href="settings.jsp?Remove=3&ProductTypeParamType=<% out.print(type.getRid()); %>">Удалить</a></td>
			</tr>				
		<%}%>
			<tr>
				<td>
					<select name="ProductParamTypeProductType[]">
						<% for(ProductType type : ControlProductType.getTypesList()){%>
							<option value="<% out.print(type.getRid()); %>"><% out.print(type.getName()); %></option>
						<%}%>
					</select>
				</td>
				<td>
					<input type="text" name="ProductTypeParamTypeName"><br>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Сохранить">
				</td>
			</tr>
		</table>		  
	</form>
	
	<form action="settings.jsp?Update=4" method = "POST" >
		<b>Действующие скидки</b>
		<table>
		<tr>
			<th>Характеристика</th>
			<th>Занчение</th>
			<th>Скидка %</th>
		</tr>	
		<% for(SpecialPrice discount : ControlSpecialPrice.getSpecialPriceList()){%>
			<tr>	
				<td>
					<select name="SpecialPriceProductParamType_<% out.print(discount.getRid()); %>">
						<% for(ProductParamType type : ControlProductParamType.getTypesList()){%>
							<option value="<% out.print(type.getRid()); %>"
							       <% if(discount.getProductParamType().getRid()==type.getRid()) out.print("selected=\"selected\""); %> >
								<% out.print(type.getName()); %>
							</option>
						<%}%>
						<% for(ProductTypeParamType type : ControlProductParamType.getTypesProductTypeList()){%>
							<option value="<% out.print(type.getRid()); %>"
							      <% if(discount.getProductParamType().getRid()==type.getRid()) out.print("selected=\"selected\""); %> >
								<% out.print(type.getName()); %>
							</option>
						<%}%>
					</select>
				</td>
				<td><input type="text" name="SpecialPriceValue_<% out.print(discount.getRid()); %>" value="<% out.println(discount.getValue()); %>"><br></td>
				<td><input type="text" name="SpecialPriceDiscount_<% out.print(discount.getRid()); %>" value="<% out.println(discount.getDiscount()); %>"><br></td>
			    <td><a href="settings.jsp?Remove=4&SpecialPrice=<% out.print(discount.getRid()); %>">Удалить</a></td>
			</tr>				
		<%}%>
			<tr>
				<td>
					<select name="SpecialPriceProductParamType">
						<% for(ProductParamType type : ControlProductParamType.getTypesList()){%>
							<option value="<% out.print(type.getRid()); %>"><% out.print(type.getName()); %></option>
						<%}%>
						<% for(ProductTypeParamType type : ControlProductParamType.getTypesProductTypeList()){%>
							<option value="<% out.print(type.getRid()); %>"><% out.print(type.getName()); %></option>
						<%}%>
					</select>
				</td>
				<td><input type="text" name="SpecialPriceValue"/> </td>
				<td><input type="text" name="SpecialPriceDiscount"/> </td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Сохранить">
				</td>
			</tr>
		</table>		  
	</form>
	
	<form action="settings.jsp?Update=5" method = "POST" >
		<b>Акции</b>
		<table>
		<tr>
			<th>Тип товара</th>
			<th>Начало</th>
			<th>Окончание</th>
		</tr>	
		<% SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
		   for(Stock stock : ControlStock.getStockList()){%>
			<tr>	
				<td>
					<select name="StockProductType_<% out.print(stock.getRid()); %>">
						<% for(ProductType type : ControlProductType.getTypesList()){%>
							<option value="<% out.print(type.getRid()); %>"
							       <% if(stock.getProductType().getRid()==type.getRid()) out.print("selected=\"selected\""); %> >
								<% out.print(type.getName()); %>
							</option>
						<%}%>
					</select>
				</td>
				<td><input type="date" name="StockBegin_<% out.print(stock.getRid()); %>" value="<% out.print(sdFormat.format(stock.getBegin())); %>"><br></td>
				<td><input type="date" name="StockEnd_<% out.print(stock.getRid()); %>" value="<% out.print(sdFormat.format(stock.getEnd())); %>"><br></td>
			    <td><a href="settings.jsp?Remove=5&Stock=<% out.print(stock.getRid()); %>">Удалить</a></td>
			</tr>				
		<%}%>
			<tr>
				<td>
					<select name="StockProductType">
						<% for(ProductType type : ControlProductType.getTypesList()){%>
							<option value="<% out.print(type.getRid()); %>"><% out.print(type.getName()); %></option>
						<%}%>
					</select>
				</td>
				<td><input type="date" name="StockBegin"/> </td>
				<td><input type="date" name="StockEnd"/> </td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Сохранить">
				</td>
			</tr>
		</table>		  
	</form>
</div>
	<a href="index.jsp">Назад</a>
</body>
</html>