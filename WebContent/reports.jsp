<%@page import="com.Otium.Shop.Controls.ControlProductType"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.Otium.Shop.Entity.Bill"%>
<%@page import="com.Otium.Shop.Controls.ControlBill"%>
<%@ page language="java" contentType="text/html; UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; UTF-8">
<title>Отчеты</title>
</head>
<body>
<div>
    <a href="shop">Касса</a>
	<a href="warehouse">Склад</a>
	<a href="reports">Отчеты</a>
	<a href="settings">Настройки</a>
</div>
<div>
    <a href="reports?Report=1">Количество проданных единиц</a>
	<a href="reports?Report=2">Типы товаров в которых более 5 наименований</a>
</div>
<%if(request.getParameter("Report")!=null){
	switch(Integer.parseInt(request.getParameter("Report"))){
		case 1:
			%>
			<table>
				<tr>
					<th>Наименование</th>
					<th>Количество</th>					
				</tr>
				<%for(Object[] row : ControlBill.getProductSells()){%>
				<tr>
					<td><% out.print(row[0]); %></td>
					<td><% out.print(row[1]); %></td>
				</tr>	
				<%}%>
			</table>
			<%
		break;
		case 2:
			%>
			<table>
				<tr>
					<th>Тип товара</th>					
				</tr>
				<%for(Object row : ControlProductType.getProductTypeMoreFive()){%>
				<tr>
					<td><% out.print(row); %></td>
				</tr>	
				<%}%>
			</table>
			<%
		break;
	}
}%>
</body>
</html>