<%@page import="javax.persistence.Entity"%>
<%@page import="com.Otium.Shop.Controls.ControlStock"%>
<%@page import="com.Otium.Shop.Controls.ControlSpecialPrice"%>
<%@page import="com.Otium.Shop.Entity.BillEntry"%>
<%@page import="com.Otium.Shop.Entity.Bill"%>
<%@page import="com.Otium.Shop.Controls.ControlBill"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.List"%>
<%@page import="com.Otium.Shop.Entity.ProductParamType"%>
<%@page import="com.Otium.Shop.Controls.ControlProduct"%>
<%@page import="com.Otium.Shop.Entity.ProductParam"%>
<%@page import="com.Otium.Shop.Controls.ControlProductParamType"%>
<%@page import="com.Otium.Shop.Entity.Product"%>
<%@page import="com.Otium.Shop.Entity.ProductType"%>
<%@page import="com.Otium.Shop.Controls.ControlProductType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
    request.setCharacterEncoding( "UTF-8" );
	Bill bill = ControlBill.getBillsList().get(0);
	
	if(request.getParameter("Bill")!=null)
		bill = ControlBill.getBill(Integer.parseInt(request.getParameter("Bill")));
	
	if(request.getParameter("Update")!=null){	
		Enumeration<String> names;
		if(request.getParameter("BillEntryProduct")!=null && !request.getParameter("BillEntryProduct").equals("")){
			out.print(request.getParameter("BillEntryProduct")+"<br>");
			out.print(request.getParameter("BillEntryQuantity")+"<br>");
			Product product = ControlProduct.getProduct(Integer.parseInt(request.getParameter("BillEntryProduct")));
			double quantity = Double.parseDouble(request.getParameter("BillEntryQuantity"));
			double price = product.getPrice();
			double discount = ControlStock.isStock(product.getType(), bill.getDate()) ? ControlSpecialPrice.getSpecialPrice(product) : 0;
			ControlBill.updateBillEntry(new BillEntry(bill,product,price,quantity,discount));			
		}
		
		names = request.getParameterNames();
		while(names.hasMoreElements()) {
			String string = names.nextElement();
			if(string.startsWith("BillEntryProduct_")){
				String id = string.substring(17);		
				BillEntry entry = ControlBill.getEntry(Integer.parseInt(id));
				if(entry!=null){
					Product product = ControlProduct.getProduct(Integer.parseInt(request.getParameter("BillEntryProduct_"+id)));
					entry.setProduct(product);
					entry.setQuantity(Double.parseDouble(request.getParameter("BillEntryQuantity_"+id)));
					entry.setPrice(product.getPrice());
					entry.setDiscount(ControlStock.isStock(product.getType(), bill.getDate()) ? ControlSpecialPrice.getSpecialPrice(product) : 0);
					ControlBill.updateBillEntry(entry);					
				}				
			}
		}
		
		response.sendRedirect("bill.jsp?Bill=" + bill.getRid());
	}
	
	if(request.getParameter("Remove")!=null){
		ControlBill.removeBillEntry(Integer.parseInt(request.getParameter("Entry")));
		response.sendRedirect("bill.jsp?Bill=" + bill.getRid());
	}	
%>
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script>
var warehouse = [
<%for(ProductType type : ControlProductType.getTypesList()){
	for(Product product : type.getProductList()){
		out.println("["+ type.getRid()    + "," + 
	                     product.getRid() + ",'" + 
		                 product.getName().replace("'", "\\'") + "'," +
	                     product.getPrice() + "," + 
		                 product.getRest().getQuantity() + "," +
		                (ControlStock.isStock(type, bill.getDate())?1:0) + "," +
		                 ControlSpecialPrice.getSpecialPrice(product) + "," +
		            "],");
	}
}%>
];
</script>
<div>
	<form action="bill.jsp?Update=yes&Bill=<% out.print(bill.getRid()); %>" method = "POST" >
		<table>
		<tr>
			<th>Тип товара</th>
			<th>Наименование</th>
			<th>Цена</th>
			<th>Количество</th>
			<th>Остаток</th>
			<th>Скидка</th>
			<th>Сумма</th>
		</tr>	
		<% for(BillEntry entry : bill.getEntry()){
		   ProductType main_type = entry.getProduct().getType();
		   %>
			<tr>
				<td>
					<%if(!bill.getStatus()) {%>
						<select class="product_type" entry=<% out.print(entry.getRid()); %>>					
							<% for(ProductType type : ControlProductType.getTypesList()){%>
								<option value="<% out.print(type.getRid()); %>"
								       <% if(main_type.getRid()==type.getRid()) out.print("selected=\"selected\""); %> >
									<% out.print(type.getName()); %>
								</option>
							<%}%>
						</select>
					<%} else { %>
					<div><% out.print(main_type.getName()); %></div>
					<%} %>
				</td>
				<td>
					<%if(!bill.getStatus()) {%>
					<select class="product product<% out.print(entry.getRid()); %>" entry=<% out.print(entry.getRid()); %> name="BillEntryProduct_<% out.print(entry.getRid()); %>">					
						<% for(Product product : main_type.getProductList()){%>
							<option value="<% out.print(product.getRid()); %>"
							       <% if(entry.getProduct().getRid()==product.getRid()) out.print("selected=\"selected\""); %> >
								<% out.print(product.getName()); %>
							</option>
						<%}%>
					</select>
					<%} else { %>
					<div><% out.print(entry.getProduct().getName()); %></div>
					<%} %>
				</td>
				<td><div class="price<% out.print(entry.getRid()); %>" ><% out.print(entry.getPrice()); %></div></td>
				<td>
					<%if(!bill.getStatus()) {%>
						<input class="quantity quantity<% out.print(entry.getRid()); %>" type="text" entry=<% out.print(entry.getRid()); %> name="BillEntryQuantity_<% out.print(entry.getRid()); %>" value="<% out.println(entry.getQuantity()); %>">
					<%} else { %>
						<div><% out.print(entry.getQuantity()); %></div>
					<%} %>
				</td>
				<td><div class="rest<% out.print(entry.getRid()); %>"><% out.print(entry.getProduct().getRest().getQuantity()); %></div></td>
				<td><div class="discount<% out.print(entry.getRid()); %>"><% out.print(entry.getDiscount()); %></div></td>
				<td><div class="sum<% out.print(entry.getRid()); %>"><% out.print(String.format("%.2f", entry.getSum())); %></div></td>
			    <td>
			    	<%if(!bill.getStatus()) {%>
						<a href="bill.jsp?Remove=1&Bill=<% out.print(bill.getRid()); %>&Entry=<% out.print(entry.getRid()); %>">Удалить</a>
					<%}%>			    
			    </td>
			</tr>				
		<%}%>
		<%if(!bill.getStatus()) {
				ProductType first_type = ControlProductType.getTypesList().get(0); %>				
			<tr>
				<td>
					<select class="product_type" entry=-1>
						<option></option>					
						<% for(ProductType type : ControlProductType.getTypesList()){%>
							<option value="<% out.print(type.getRid()); %>">
								<% out.print(type.getName()); %>
							</option>
						<%}%>
					</select>
				</td>
				<td>
					<select class="product product-1" entry=-1 name="BillEntryProduct">
						<option></option>					
						<% for(Product product : first_type.getProductList()){%>
							<option value="<% out.print(product.getRid()); %>">
								<% out.print(product.getName()); %>
							</option>
						<%}%>
					</select>
				</td>
				<td><div class="price-1" >0.0</div></td>
				<td><input class="quantity quantity-1" type="text" entry=-1 name="BillEntryQuantity"><br></td>
				<td><div class="rest-1">0.0</div></td>
				<td><div class="discount-1">0.0</div></td>
				<td><div class="sum-1">0.0</div></td>			
			    <td></td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Сохранить">
				</td>
			</tr>
		<% } %>
		</table>		  
	</form>
	<div class="test"></div>
</div>
<script type="text/javascript">
function getSum(entry){
	var quantity =  parseFloat($('.quantity' + entry).val());
	var price = parseFloat($('.price' + entry).text());
	var discount = parseFloat($('.discount' + entry).text());
	return quantity * price * (1 - (discount/100));
};

$('select.product_type').on('change', function(){
	var options = [];
	self = this;
	$.each(warehouse, function(index, product){
		if(product[0]==self.value)		
			options.push({value:product[1], 
				          text:product[2] ,
				          price:product[3],
				          rest:product[4],
				          isStock:product[5],
				          discount:product[6]})
			
	});
	$(".product" + this.getAttribute("entry")).replaceOptions(options);
	$('.price' + this.getAttribute("entry")).html(options[0].price);
	$('.rest' + this.getAttribute("entry")).html(options[0].rest);
	if(options[0].isStock==1)
		$('.discount' + this.getAttribute("entry")).html(options[0].discount);
	$('.sum' + this.getAttribute("entry")).text(getSum(this.getAttribute("entry")));
});

$('select.product').on('change', function(){
	self = this;
	$.each(warehouse, function(index, product){
		if(product[1]==self.value){		
			$('.price' + self.getAttribute("entry")).html(product[3]);
			$('.rest' + self.getAttribute("entry")).html(product[4]);
			if(product[5]==1)
				$('.discount' + self.getAttribute("entry")).html(product[6]);
			return;
		}
	});
	$('.sum' + this.getAttribute("entry")).text(getSum(this.getAttribute("entry")));
});

$('input.quantity').on('change', function(){
	$('.sum' + this.getAttribute("entry")).text(getSum(this.getAttribute("entry")));		
});
</script>