<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.Otium.Shop.Entity.Bill"%>
<%@page import="com.Otium.Shop.Controls.ControlBill"%>
<%@ page language="java" contentType="text/html; UTF-8" pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
	if(request.getParameter("AddBill")!=null){
		Bill bill = new Bill(new Date());
		ControlBill.updateBill(bill);
		response.sendRedirect("shop");
	}		
	
	if(request.getParameter("Execute")!=null){
		ControlBill.excecuteBill(Integer.parseInt(request.getParameter("Bill")));
		response.sendRedirect("shop");
	}
	
	if(request.getParameter("Remove")!=null){
		ControlBill.removeBill(Integer.parseInt(request.getParameter("Bill")));
		response.sendRedirect("shop");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; UTF-8">
<title>Касса</title>
</head>
<body>
<div>
    <a href="shop">Касса</a>
	<a href="warehouse">Склад</a>
	<a href="reports">Отчеты</a>
	<a href="settings">Настройки</a>
</div>
<div style="display: flex;">
	<div style="width: 40%;">
		<table>
		<tr>
			<th>№ чека</th>
			<th>Дата</th>
			<th>Сумма</th>
			<th>Статус</th>
		</tr>
		<%SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
		  for(Bill bill : ControlBill.getBillsList()) { %>
			<tr>
				<td><% out.print(bill.getRid()); %></td>
				<td><input type="date" name="BillDate_<% out.print(bill.getRid()); %>" value="<% out.print(sdFormat.format(bill.getDate())); %>"></td>
				<td><% out.print(String.format("%.2f", ControlBill.getSumOfBill(bill), Locale.US)); %></td>
				<%if(!bill.getStatus()){%>
					<td><a href="shop.jsp?Execute=yes&Bill=<% out.print(bill.getRid()); %>">Провести</a></td>
				<%} else {%>
					<td>Проведен</td>
				<%} %>
				<td><a href="bill.jsp?Bill=<% out.print(bill.getRid()); %>" target="iframe_bill">Просмотр</a></td>
				<%if(!bill.getStatus()){%>
					<td><a href="shop.jsp?Remove=yes&Bill=<% out.print(bill.getRid()); %>">Удалить</a></td>
				<%} else {%>
					<td>Удалить</td>
				<%} %>
			</tr>
		<%} %>
		</table>
	</div>
	<div style="width: 60%">
		<iframe name="iframe_bill" Width="100%" frameborder=0 scrolling=auto>
		</iframe>
	</div>
</div>
<a href="shop.jsp?AddBill=yes">Создать чек</a>
</body>
</html>