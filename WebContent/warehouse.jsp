<%@page import="com.Otium.Shop.Entity.ProductType"%>
<%@page import="com.Otium.Shop.Controls.ControlProductType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Склад</title>
</head>
<body>
	<div>
	    <a href="shop">Касса</a>
		<a href="warehouse">Склад</a>
		<a href="reports">Отчеты</a>
		<a href="settings">Настройки</a>
	</div>
	<div style="display: flex;">		
		<div style="width: 10%; padding: 10px">
			<b>Тип товара</b>			
			<% for(ProductType type : ControlProductType.getTypesList()) { %>
				<div>
					<a href="production.jsp?ProductType=<% out.print(type.getRid()); %>" target="iframe_production"><% out.print(type.getName()); %></a>
				</div>				
			<% } %>
		</div>
		<div style="width: 90%">
			<iframe src="production.jsp" name="iframe_production" Width="100%" height="120%" frameborder=0 scrolling=auto>
			</iframe>
		</div>
	</div>
	<a href="index.jsp">Назад</a>
</body>
</html>