package com.Otium.Shop.Entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Entity implementation class for Entity: SpecialPrice
 *
 */
@Entity

public class SpecialPrice implements Serializable {

	   
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int rid;
	private ProductParamType ProductParamType;
	private String Value;
	private double Discount;
	private static final long serialVersionUID = 1L;

	public SpecialPrice() {
		super();
	}   
	public int getRid() {
		return this.rid;
	}

	public SpecialPrice(ProductParamType productType, String value, double discount) {
		super();
		ProductParamType = productType;
		Value = value;
		Discount = discount;
	}
	
	public void setRid(int rid) {
		this.rid = rid;
	}   
	
	public ProductParamType getProductParamType() {
		return this.ProductParamType;
	}

	public void setProductParamType(ProductParamType ProductType) {
		this.ProductParamType = ProductType;
	}   
	public String getValue() {
		return this.Value;
	}

	public void setValue(String Value) {
		this.Value = Value;
	}   
	public double getDiscount() {
		return this.Discount;
	}

	public void setDiscount(double Discount) {
		this.Discount = Discount;
	}
   
}
