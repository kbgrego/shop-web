package com.Otium.Shop.Entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity implementation class for Entity: Stock
 *
 */
@Entity(name="Stock")

public class Stock implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int rid;
	private ProductType ProductType;
	@Temporal(TemporalType.DATE)
	private Date begin;
	@Temporal(TemporalType.DATE)
	private Date end;
	private static final long serialVersionUID = 1L;

	public Stock() {
		super();
	}   

	public Stock(ProductType productType, Date begin, Date end) {
		super();
		ProductType = productType;
		this.begin = begin;
		this.end = end;
	}

	public int getRid() {
		return this.rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}   
	
	@ManyToOne
	public ProductType getProductType() {
		return this.ProductType;
	}

	public void setProductType(ProductType ProductType) {
		this.ProductType = ProductType;
	}   
	public Date getBegin() {
		return this.begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}   
	public Date getEnd() {
		return this.end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
   
}
