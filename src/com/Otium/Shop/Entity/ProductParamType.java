package com.Otium.Shop.Entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Entity implementation class for Entity: ProductParamType
 *
 */
@Entity

public class ProductParamType implements Serializable {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int rid;
	private String Name;
	private static final long serialVersionUID = 1L;

	public ProductParamType() {
		super();
	}   
	public ProductParamType(String name) {
		this.Name = name;
	}
	public int getRid() {
		return this.rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}   
	public String getName() {
		return this.Name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + rid;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductParamType other = (ProductParamType) obj;
		if (rid != other.rid)
			return false;
		return true;
	}
	public void setName(String Name) {
		this.Name = Name;
	}
   
	public String toString(){
		return this.Name;
	}
}
