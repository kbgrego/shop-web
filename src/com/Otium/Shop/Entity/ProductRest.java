package com.Otium.Shop.Entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Entity implementation class for Entity: ProductRest
 *
 */
@Entity

public class ProductRest implements Serializable {
	   
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int rid;
	@OneToOne
	private Product Product;
	private double Quantity;
	private static final long serialVersionUID = 1L;

	public ProductRest() {
		super();
	}   
	public ProductRest(Product product, double quantity) {
		this.Product = product;
		this.Quantity = quantity;
	}
	public int getRid() {
		return this.rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}   
	
	@OneToOne
	public Product getProduct() {
		return this.Product;
	}

	public void setProduct(Product Product) {
		this.Product = Product;
	}   
	public double getQuantity() {
		return this.Quantity;
	}

	public void setQuantity(double Quantity) {
		this.Quantity = Quantity;
	}
   
}
