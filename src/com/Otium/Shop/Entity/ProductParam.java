package com.Otium.Shop.Entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Entity implementation class for Entity: ProductParam
 *
 */
@Entity

public class ProductParam implements Serializable {

	   
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int rid;
	@JoinColumn(nullable=false) 
	@ManyToOne
	private Product Product;
	@JoinColumn(nullable=false) 
	private ProductParamType ProductParam;
	private String Value;
	private static final long serialVersionUID = 1L;

	public ProductParam() {
		super();
	}
	
	public int getRid() {
		return this.rid;
	}

	public ProductParam(ProductParamType productParam, Product product) {
		super();
		Product = product;
		ProductParam = productParam;
	}
	
	public ProductParam(Product product, ProductParamType productParam, String value) {
		super();
		Product = product;
		ProductParam = productParam;
		Value = value;
	}
	
	public void setRid(int rid) {
		this.rid = rid;
	}   
	
	@ManyToOne
	public Product getProduct() {
		return this.Product;
	}

	public void setProduct(Product Product) {
		this.Product = Product;
	}   
	
	public ProductParamType getProductParam() {
		return this.ProductParam;
	}

	public void setProductParam(ProductParamType ProductParam) {
		this.ProductParam = ProductParam;
	}   
	public String getValue() {
		return this.Value;
	}

	public void setValue(String Value) {
		this.Value = Value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Product == null) ? 0 : Product.hashCode());
		result = prime * result + ((ProductParam == null) ? 0 : ProductParam.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductParam other = (ProductParam) obj;
		if (Product == null) {
			if (other.Product != null)
				return false;
		} else if (!Product.equals(other.Product))
			return false;
		if (ProductParam == null) {
			if (other.ProductParam != null)
				return false;
		} else if (!ProductParam.equals(other.ProductParam))
			return false;
		return true;
	}
   
}
