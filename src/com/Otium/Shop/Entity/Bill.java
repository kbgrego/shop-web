package com.Otium.Shop.Entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity implementation class for Entity: Bill
 *
 */
@Entity

public class Bill implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int rid;
	@Column(nullable=false) 
	@Temporal(TemporalType.DATE)
	private java.util.Date Date;
	private boolean Status;
	
	@OneToMany(mappedBy = "Bill", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<BillEntry> Entry;

	public Bill() {
		super();
	}   
	
	
	public Bill(java.util.Date date) {
		super();
		Date = date;
	}


	public int getRid() {
		return this.rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}   
	public java.util.Date getDate() {
		return this.Date;
	}

	public void setDate(java.util.Date Date) {
		this.Date = Date;
	}   
	public boolean getStatus() {
		return this.Status;
	}

	public void setStatus(boolean Status) {
		this.Status = Status;
	}
		
	public Collection<BillEntry> getEntry() {
		return Entry;
	}   
}
