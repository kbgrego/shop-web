package com.Otium.Shop.Entity;

import java.io.Serializable;

import javax.persistence.Entity;

/**
 * Entity implementation class for Entity: ProductTypeParamType
 *
 */
@Entity

public class ProductTypeParamType extends ProductParamType implements Serializable {

	private ProductType ProductType;
	private static final long serialVersionUID = 1L;

	public ProductTypeParamType() {
		super();
	}   

	public ProductTypeParamType(ProductType product_type, String name) {
		this.ProductType = product_type;
		setName(name);
	}

	public ProductType getProductType() {
		return this.ProductType;
	}

	public void setProductType(ProductType ProductType) {
		this.ProductType = ProductType;
	}
   
}
