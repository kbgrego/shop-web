package com.Otium.Shop.Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * Entity implementation class for Entity: Product
 *
 */
@Entity

public class Product implements Serializable {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int rid;
	@JoinColumn(nullable=false) 
	private String Name;
	@JoinColumn(nullable=false)
	@ManyToOne
	private ProductType ProductType;
	private double Price;
	
	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true)
	private ProductRest Rest;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Product", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<ProductParam> Parameters;
	                
	private static final long serialVersionUID = 1L;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + rid;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (rid != other.rid)
			return false;
		return true;
	}

	public Product() {
		super();
		Rest = new ProductRest(this, 0);
	}   
	
	public Product(String name, ProductType type) {
		this();
		Name = name;
		ProductType = type;
	}

	public int getRid() {
		return this.rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}   
	public String getName() {
		return this.Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}   
	
	@ManyToOne
	public ProductType getType() {
		return this.ProductType;
	}
	
	public void setType(ProductType Type) {
		this.ProductType = Type;
	}   
	public double getPrice() {
		return this.Price;
	}

	public void setPrice(double Price) {
		this.Price = Price;
	}

	public ProductRest getRest() {
		return Rest;
	}

	public void setRest(ProductRest rest) {
		Rest = rest;
	}

	public Collection<ProductParam> getParameters() {
		if(Parameters==null)
			Parameters = new ArrayList<>();
		return Parameters;
	}

	@Override
	public String toString() {
		return Name;
	}   
}
