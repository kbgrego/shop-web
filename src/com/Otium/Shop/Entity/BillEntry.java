package com.Otium.Shop.Entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Entity implementation class for Entity: BillEntry
 *
 */
@Entity

public class BillEntry implements Serializable {
	private static final long serialVersionUID = -635276930581861604L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int rid;
	@JoinColumn(nullable=false) 
	@ManyToOne
	private Bill    Bill;
	@JoinColumn(nullable=false) 
	private Product Product;
	private double Price;	
	private double Quantity;
	private double Discount;
	
	public BillEntry() {
		super();
	}
	
	public BillEntry(Bill bill) {
		super();
		Bill = bill;
	}   
	
	public BillEntry(Bill bill, Product product, double price, double quantity, double discount) {
		super();
		Bill = bill;
		Product = product;
		Price = price;
		Quantity = quantity;
		Discount = discount;
	}

	public int getRid() {
		return this.rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}   
	
	public Bill getBill() {
		return Bill;
	}
	public void setBill(Bill bill) {
		Bill = bill;
	}
	public Product getProduct() {
		return this.Product;
	}

	public void setProduct(Product Product) {
		this.Product = Product;
	}   
	public double getPrice() {
		return this.Price;
	}

	public void setPrice(double Price) {
		this.Price = Price;
	}   
	public double getQuantity() {
		return this.Quantity;
	}

	public void setQuantity(double Quantity) {
		this.Quantity = Quantity;
	}   
	public double getDiscount() {
		return this.Discount;
	}

	public void setDiscount(double Discount) {
		this.Discount = Discount;
	}
	
	public double getSum(){
		return getQuantity() * getPrice() * (1 - (getDiscount() / 100));
	}   
}
