package com.Otium.Shop.Entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: ProductType
 *
 */
@Entity
@Table
@NamedQuery(query= "Select e from ProductType e", name = "get all product types")

public class ProductType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int rid;
	private String name;
	private static final long serialVersionUID = 1L;
	
	@OneToMany(mappedBy = "ProductType", orphanRemoval=true, cascade=CascadeType.ALL)
	private Collection<Product> Products;

	public ProductType() {
		super();
	}
	public ProductType(String name) {
		super();
		this.name = name;
	}
	public ProductType(int rid, String name) {
		super();
		this.rid = rid;
		this.name = name;
	}
	public int getRid() {
		return this.rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addProduct(Product product) {
		if (!Products.contains(product)) {
			Products.add(product);
		}
	}
	
	public Collection<Product> getProductList() {
		return Products;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + rid;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductType other = (ProductType) obj;
		if (rid != other.rid)
			return false;
		return true;
	}
	@Override
	public String toString(){
		return name;
	}
   
}
