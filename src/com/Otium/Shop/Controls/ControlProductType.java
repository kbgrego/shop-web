package com.Otium.Shop.Controls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.Otium.Shop.Database.DatabaseManager;
import com.Otium.Shop.Entity.Product;
import com.Otium.Shop.Entity.ProductParam;
import com.Otium.Shop.Entity.ProductParamType;
import com.Otium.Shop.Entity.ProductType;

public class ControlProductType {
	
	public static List<ProductType> getTypesList(){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		Query query = entitymanager.createNamedQuery("get all product types");
		
		return query.getResultList();		
	}
	
	public static ProductType getType(int rid){
		ProductType type=null;
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
				
		try {
			type = entitymanager.find(ProductType.class, rid);
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		entitymanager.close();		
		return type;
	}
	
	public static List<ProductParamType> getProductParamTypes(ProductType productType) {
		List<ProductParamType> list = new ArrayList<>();
		list = ControlProductParamType.getTypesList();
		list.addAll(ControlProductParamType.getTypesProductTypeList(productType));
		Collections.sort(list, new Comparator<ProductParamType>() {
			@Override
			public int compare(ProductParamType o1, ProductParamType o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		return list;
	}
	
	public static void addType(String name) {	
		if(name==null || name.equals(""))
			return;
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		entitymanager.getTransaction().begin();
		
		entitymanager.persist(new ProductType(name));
		
		entitymanager.getTransaction().commit();
		entitymanager.close();
		
	}
	
	public static void updateType(int rid, String name) {		
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			ProductType type = entitymanager.find(ProductType.class, rid);
			type.setName(name);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			e.printStackTrace();
		}	
		
		entitymanager.close();
		
	}
	
	public static void updateType(ProductType type) {
		if(type.getRid()!=0)
			updateType(type.getRid(), type.getName());
		else
			addType(type.getName());
	}
	
	public static void removeType(int i){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		try{
			entitymanager.getTransaction().begin();
			
			ProductType type = entitymanager.find(ProductType.class, i);
			entitymanager.remove(type);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
		}
		entitymanager.close();
		
	}

	public static List<Object> getProductTypeMoreFive() {
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		Query query = entitymanager.createQuery("select f from ProductType f where (select count(p) from Product p where p.ProductType = f) > 5");
		return query.getResultList();	
	}
	
}
