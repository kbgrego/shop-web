package com.Otium.Shop.Controls;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.Otium.Shop.Database.DatabaseManager;
import com.Otium.Shop.Entity.Product;
import com.Otium.Shop.Entity.ProductParamType;
import com.Otium.Shop.Entity.SpecialPrice;

public class ControlSpecialPrice {
	
	public static List<SpecialPrice> getSpecialPriceList(){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		Query query = entitymanager.createQuery("select f from SpecialPrice f");
		
		return query.getResultList();		
	}
	
	public static void addSpecialPrice(ProductParamType type, String value, double discount) {		
		updateSpecialPrice(new SpecialPrice(type, value, discount));
	}
	
	public static void addSpecialPrice(SpecialPrice s_price) {
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		entitymanager.getTransaction().begin();
		
		entitymanager.persist(s_price);
		
		entitymanager.getTransaction().commit();
		entitymanager.close();
		
	}
		
	public static void addSpecialPrice(int product_param_type_rid, String value, double discount) {
		if(product_param_type_rid==0 || value==null || value.equals("") || discount==0)
			return;
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		entitymanager.getTransaction().begin();
		
		ProductParamType param_type = entitymanager.find(ProductParamType.class, product_param_type_rid);		
		entitymanager.persist(new SpecialPrice(param_type, value, discount));

		entitymanager.getTransaction().commit();
		entitymanager.close();
		
	}

	public static void updateSpecialPrice(SpecialPrice s_price) {
		if(s_price.getRid()!=0)
			updateSpecialPrice(s_price.getRid(), s_price.getProductParamType(), s_price.getValue(),s_price.getDiscount());
		else
			addSpecialPrice(s_price);
	}
	
	public static void updateSpecialPrice(int rid, ProductParamType type, String value, double discount) {		
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			SpecialPrice special_price = entitymanager.find(SpecialPrice.class, rid);
			special_price.setProductParamType(type);
			special_price.setValue(value);
			special_price.setDiscount(discount);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			e.printStackTrace();
		}	
		
		entitymanager.close();
		
	}
	
	
	public static void updateSpecialPrice(int rid, int product_param_type_rid, String value, double discount) {		
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			ProductParamType param_type = entitymanager.find(ProductParamType.class, product_param_type_rid);
			SpecialPrice special_price = entitymanager.find(SpecialPrice.class, rid);
			special_price.setProductParamType(param_type);
			special_price.setValue(value);
			special_price.setDiscount(discount);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			e.printStackTrace();
		}	
		
		entitymanager.close();
		
	}

	public static void removeSpecialPrice(int i){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		try{
			entitymanager.getTransaction().begin();
			
			SpecialPrice special_price = entitymanager.find(SpecialPrice.class, i);
			entitymanager.remove(special_price);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
		}
		entitymanager.close();
		
	}

	public static double getSpecialPrice(Product n) {
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		Query query = entitymanager.createQuery("select a.Discount from SpecialPrice a, ProductParam b where a.ProductParamType = b.ProductParam and a.Value = b.Value and b.Product = :product");
		query.setParameter("product", n);
		
		try {
			Object object = query.getSingleResult();
			return object==null ? 0 : (Double) object;
		} catch (Exception e){
			return 0;
		}
	}
}
