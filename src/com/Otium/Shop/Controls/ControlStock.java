package com.Otium.Shop.Controls;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import com.Otium.Shop.Database.DatabaseManager;
import com.Otium.Shop.Entity.ProductType;
import com.Otium.Shop.Entity.Stock;

public class ControlStock {
	
	public static List<Stock> getStockList(){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		Query query = entitymanager.createQuery("select a from Stock a");
		
		return query.getResultList();		
	}
	
	public static void addStock(ProductType type, Date begin_date, Date end_date) {		
		updateStock(new Stock(type, begin_date, end_date));
	}
	
	public static void addStock(Stock stock) {
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		entitymanager.getTransaction().begin();
		
		entitymanager.persist(stock);
		
		entitymanager.getTransaction().commit();
		entitymanager.close();
		
	}
	
	
	public static void addStock(int product_type_id, Date begin_date, Date end_date) {
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		entitymanager.getTransaction().begin();
		
		ProductType type = entitymanager.find(ProductType.class, product_type_id);
		Stock stock = new Stock(type, begin_date, end_date);
		entitymanager.persist(stock);
		
		entitymanager.getTransaction().commit();
		entitymanager.close();
		
	}


	public static void updateStock(Stock stock) {
		if(stock.getRid()!=0)
			updateStock(stock.getRid(), stock.getProductType(), stock.getBegin(), stock.getEnd());
		else
			addStock(stock);
	}
	
	public static void updateStock(int rid, int product_type_id, Date begin_date, Date end_date) {		
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			ProductType type = entitymanager.find(ProductType.class, product_type_id);
			Stock stock = entitymanager.find(Stock.class, rid);
			stock.setProductType(type);
			stock.setBegin(begin_date);
			stock.setEnd(end_date);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			e.printStackTrace();
		}	
		
		entitymanager.close();
		
	}
	
	public static void updateStock(int rid, ProductType type, Date begin_date, Date end_date) {		
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			Stock stock = entitymanager.find(Stock.class, rid);
			stock.setProductType(type);
			stock.setBegin(begin_date);
			stock.setEnd(end_date);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			e.printStackTrace();
		}	
		
		entitymanager.close();
		
	}

	public static void removeStock(int i){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		try{
			entitymanager.getTransaction().begin();
			
			Stock stock = entitymanager.find(Stock.class, i);
			entitymanager.remove(stock);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
		}
		entitymanager.close();
		
	}

	public static boolean isStock(ProductType type, Date date) {
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		Query query = entitymanager.createQuery("select f from Stock f where f.ProductType=:type and f.begin<=:date and f.end>=:date");
		query.setParameter("type", type);
		query.setParameter("date", date, TemporalType.DATE);

		try{
			return query.getSingleResult()!=null;
		} catch (Exception e){
			return false;
		}
	}
}
