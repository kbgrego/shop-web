package com.Otium.Shop.Controls;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.Otium.Shop.Database.DatabaseManager;
import com.Otium.Shop.Entity.Bill;
import com.Otium.Shop.Entity.BillEntry;
import com.Otium.Shop.Entity.Product;

public class ControlBill {
	
	public static Bill getBill(int rid){
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			Bill bill = entitymanager.find(Bill.class, rid);
			return bill;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			entitymanager.close();
		}
	}
	
	public static List<Bill> getBillsList(){
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		Query query = entitymanager.createQuery("select f from Bill f");
		
		return query.getResultList();		
	}
	
	public static double getSumOfBill(Bill bill){
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		Query query = entitymanager.createQuery("select sum(f.Quantity * f.Price * (1 - f.Discount/100)) from BillEntry f where f.Bill = :bill");
		query.setParameter("bill", bill);
		Object object = query.getSingleResult();
		return object==null ? 0 : (Double) object;
	}

	public static boolean excecuteBill(int rid){
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		Bill bill = entitymanager.find(Bill.class, rid);
		if(bill.getStatus() || bill.getEntry().size()==0)
			return false;
		
		try{
			entitymanager.getTransaction().begin();
			for(BillEntry entry : bill.getEntry()){
				Product product = entitymanager.find(Product.class, entry.getProduct().getRid());
				double rest = product.getRest().getQuantity();
				if(rest - entry.getQuantity() < 0)
					throw new Exception("Not enough in rest");
				else if(entry.getQuantity() <= 0)
					throw new Exception("Invalid quantity in bill");
				else
					product.getRest().setQuantity(rest - entry.getQuantity());
			}
			bill.setStatus(true);
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			return false;
		}

		return true;
	}	

	public static void updateBill(Bill bill) {		
		if(bill.getStatus())
			return;
		if(bill.getRid()!=0)
			updateBill(bill.getRid(), bill.getDate(), bill.getStatus());
		else
			addBill(bill);
	}
	
	private static void addBill(Bill bill) {
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		entitymanager.getTransaction().begin();
		
		entitymanager.persist(bill);
		
		entitymanager.getTransaction().commit();
		entitymanager.close();
	}


	private static void updateBill(int rid, Date date, boolean status) {
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			Bill bill = entitymanager.find(Bill.class, rid);
			if(bill.getStatus()){
				entitymanager.getTransaction().rollback();
				entitymanager.close();
				return;
			}
			bill.setDate(date);
			bill.setStatus(status);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			e.printStackTrace();
		}	
		
		entitymanager.close();
	}

	public static void removeBill(int rid) {
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		try{
			entitymanager.getTransaction().begin();
			
			Bill bill = entitymanager.find(Bill.class, rid);
			if(bill.getStatus()){
				entitymanager.getTransaction().rollback();
				entitymanager.close();
				return;
			}
			entitymanager.remove(bill);		
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			entitymanager.getTransaction().rollback();
		}
		entitymanager.close();
	}
	
	public static BillEntry getEntry(int rid){
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		try {			
			return entitymanager.find(BillEntry.class, rid);				
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}	finally {
			entitymanager.close();	
		}
	}

	public static void updateBillEntry(BillEntry entry) {
		if(entry.getProduct()==null || entry.getQuantity()==0 || entry.getBill().getStatus()) 
			return;
		if(entry.getRid()!=0)
			updateBillEntry(entry.getRid(), entry.getProduct(), entry.getQuantity(), entry.getPrice(), entry.getDiscount());
		else
			addBillEntry(entry);
	}
	
	
	private static void addBillEntry(BillEntry entry) {
		if(entry.getBill().getStatus())
			return;
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		entitymanager.getTransaction().begin();
		
		Bill bill = entitymanager.find(Bill.class, entry.getBill().getRid());
		bill.getEntry().add(entry);
		
		
		entitymanager.getTransaction().commit();
		entitymanager.close();
	}


	private static void updateBillEntry(int rid, Product product, double quantity, double price, double discount) {
		if(rid==0 || product==null || quantity<=0 || price<=0) 
			return;

		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			BillEntry entry = entitymanager.find(BillEntry.class, rid);
			if(entry.getBill().getStatus()){
				entitymanager.getTransaction().rollback();
				entitymanager.close();
				return;
			}
			entry.setProduct(product);
			entry.setQuantity(quantity);
			entry.setPrice(price);
			entry.setDiscount(discount);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			e.printStackTrace();
		}	
		
		entitymanager.close();
	}

	public static void removeBillEntry(int rid) {
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		try{
			entitymanager.getTransaction().begin();
						
			BillEntry entry = entitymanager.find(BillEntry.class, rid);
			if(entry.getBill().getStatus()){
				entitymanager.getTransaction().rollback();
				entitymanager.close();
				return;
			}
			
			entry.getBill().getEntry().remove(entry);		
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			entitymanager.getTransaction().rollback();
		}
		entitymanager.close();
	}
	
	public static List<Object[]> getProductSells(){
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("ShopDatabase");
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		Query query = entitymanager.createQuery("select f.Product, sum(f.Quantity) from BillEntry f, Bill b where f.Bill = b and b.Status=1 group by f.Product");
		return query.getResultList();	
	}
}
