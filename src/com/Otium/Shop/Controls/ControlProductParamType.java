package com.Otium.Shop.Controls;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.Otium.Shop.Database.DatabaseManager;
import com.Otium.Shop.Entity.ProductParamType;
import com.Otium.Shop.Entity.ProductType;
import com.Otium.Shop.Entity.ProductTypeParamType;

public class ControlProductParamType {
	
	public static List<ProductParamType> getTypesList(){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		Query query = entitymanager.createQuery("select f from ProductParamType f WHERE not EXISTS (select a from ProductTypeParamType a where f.rid = a.rid)");
		
		return query.getResultList();		
	}
	
	public static void addType(String name){
		if(name==null || name.equals(""))
			return;
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		entitymanager.getTransaction().begin();
		
		entitymanager.persist(new ProductParamType(name));
		
		entitymanager.getTransaction().commit();
		entitymanager.close();
		
	}
	
	public static void addType(ProductParamType type){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		entitymanager.getTransaction().begin();
		
		entitymanager.persist(type);
		
		entitymanager.getTransaction().commit();
		entitymanager.close();
		
	}
	
	public static void updateType(ProductParamType type) {
		if(type.getRid()!=0)
			updateType(type.getRid(), type.getName());
		else
			addType(type);
	}

	public static void updateType(int rid, String name) {		
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			ProductParamType type = entitymanager.find(ProductParamType.class, rid);
			type.setName(name);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			e.printStackTrace();
		}	
		
		entitymanager.close();
		
	}
	
	public static List<ProductTypeParamType> getTypesProductTypeList(){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		Query query = entitymanager.createQuery("select f from ProductTypeParamType f");
		
		return query.getResultList();		
	}
	
	public static List<ProductParamType> getTypesProductTypeList(ProductType type){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		type = entitymanager.find(ProductType.class, type.getRid());
		Query query = entitymanager.createQuery("select f from ProductTypeParamType f where f.ProductType = :type");
		query.setParameter("type", type);
		
		return query.getResultList();		
	}
	
	public static void addType(ProductTypeParamType product_type_param_type){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		try{
			entitymanager.getTransaction().begin();
					
			entitymanager.persist(product_type_param_type);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
		}
		entitymanager.close();
		
	}
	
	public static void addType(ProductType product_type, String name){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		try{
			entitymanager.getTransaction().begin();
					
			entitymanager.persist(new ProductTypeParamType(product_type, name));
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
		}
		entitymanager.close();
		
	}
	
	public static void addType(int product_type_rid, String name){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		try{
			entitymanager.getTransaction().begin();
			ProductType product_type = entitymanager.find(ProductType.class, product_type_rid);
			entitymanager.persist(new ProductTypeParamType(product_type, name));
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
		}
		entitymanager.close();
		
	}
	
	public static void removeType(int i){
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		try{
			entitymanager.getTransaction().begin();
			
			ProductParamType type = entitymanager.find(ProductParamType.class, i);
			entitymanager.remove(type);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
		}
		entitymanager.close();
		
	}

	public static void updateType(int rid, ProductType product_type, String name) {
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			ProductTypeParamType type = entitymanager.find(ProductTypeParamType.class, rid);
			type.setProductType(product_type);
			type.setName(name);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			e.printStackTrace();
		}	
		
		entitymanager.close();
		
	}
	
	public static void updateType(ProductTypeParamType type) {
		if(type.getRid()!=0)
			updateType(type.getRid(), type.getProductType(), type.getName());
		else
			addType(type);
	}
}
