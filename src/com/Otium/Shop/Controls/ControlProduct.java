package com.Otium.Shop.Controls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.Otium.Shop.Database.DatabaseManager;
import com.Otium.Shop.Entity.Product;
import com.Otium.Shop.Entity.ProductParam;
import com.Otium.Shop.Entity.ProductParamType;
import com.Otium.Shop.Entity.ProductRest;
import com.Otium.Shop.Entity.ProductType;

public class ControlProduct {
	
	public static List<Product> getProductList(){
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		Query query = entitymanager.createQuery("select f from Product f");
		
		return query.getResultList();		
	}
	
	public static Product getProduct(int rid){
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		try {			
			return entitymanager.find(Product.class, rid);				
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}	finally {
			entitymanager.close();	
		}		 
	}
	
	public static void addProduct(Product product) {
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			ProductType type = entitymanager.find(ProductType.class, product.getType().getRid());
			type.getProductList().add(product);
			
			entitymanager.getTransaction().commit();		
		} catch (Exception e) {
			e.printStackTrace();
			entitymanager.getTransaction().rollback();
		}	
		entitymanager.close();
	}
	
	
	public static void addProduct(String name, ProductType type) {
		if(type==null || name==null || name.equals(""))
			return ;
		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		entitymanager.getTransaction().begin();
		
		entitymanager.persist(new Product(name, type));
		
		entitymanager.getTransaction().commit();
		entitymanager.close();
	}
	
	public static void updateProduct(int rid, String name, ProductType type, double price) {		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			Product product = entitymanager.find(Product.class, rid);
			
			product.setName(name);
			product.setType(type);
			product.setPrice(price);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			e.printStackTrace();
		}	
		
		entitymanager.close();
	}
	
	public static void updateProduct(int rid, String name, ProductType type, double price, double rest) {		
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			Product product = entitymanager.find(Product.class, rid);
			
			product.setName(name);
			product.setType(type);
			product.setPrice(price);
			product.getRest().setQuantity(rest);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			e.printStackTrace();
		}	
		
		entitymanager.close();
	}
	
	public static void updateProduct(Product product) {
		if(product.getRid()!=0)
			updateProduct(product.getRid(), product.getName(), product.getType(), product.getPrice(), product.getRest().getQuantity());
		else
			addProduct(product);
	}
	
	public static void removeProduct(int i){
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("ShopDatabase");
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		try{
			entitymanager.getTransaction().begin();
			
			Product product = entitymanager.find(Product.class, i);
			product.getType().getProductList().remove(product);
			/*entitymanager.remove(product);*/		
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			entitymanager.getTransaction().rollback();
		}
		entitymanager.close();
		emfactory.close();
	}


	public static void updateProductRest(ProductRest rest) {
		if(rest.getRid()!=0)
			updateProductRest(rest.getRid(), rest.getQuantity());
		else
			addProductRest(rest);
	}


	private static void addProductRest(ProductRest rest) {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("ShopDatabase");
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		entitymanager.getTransaction().begin();
		
		entitymanager.persist(rest);
		
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();	
	}


	private static void updateProductRest(int rid, double quantity) {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("ShopDatabase");
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			ProductRest rest = entitymanager.find(ProductRest.class, rid);
			
			rest.setQuantity(quantity);
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			e.printStackTrace();
		}	
		
		entitymanager.close();
		emfactory.close();
	}


	public static List<ProductParam> getProductParam(Product product) {
		Set<ProductParam> set = new HashSet<>();
		if(product.getParameters()!=null)
			set.addAll(product.getParameters());
		set.addAll(getProductEmptyParam(product.getType(), product));
		List<ProductParam> list = new ArrayList<>(set);
		Collections.sort(list, new Comparator<ProductParam>() {
			@Override
			public int compare(ProductParam o1, ProductParam o2) {
				return o1.getProductParam().getName().compareTo(o2.getProductParam().getName());
			}
		});
		return list;
	}


	private static List<ProductParam> getProductEmptyParam(ProductType productType, Product product) {
		List<ProductParam> list = new ArrayList<>();
		ControlProductType.getProductParamTypes(productType).forEach(t -> list.add(new ProductParam(t,product)));
		return list;
	}
	
	public static void updateProductParam(ProductParam param) {
		if(param.getRid()!=0)
			updateProductParam(param.getRid(), param.getProductParam(), param.getProduct(), param.getValue());
		else
			addProductParam(param);
	}
	
	private static void addProductParam(ProductParam param) {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("ShopDatabase");
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		entitymanager.getTransaction().begin();
		
		Product product = entitymanager.find(Product.class, param.getProduct().getRid());
		product.getParameters().add(param);
		
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();	
	}

	private static void addProductParam(ProductParamType type, Product product, String value) {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("ShopDatabase");
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		entitymanager.getTransaction().begin();
		
		product = entitymanager.find(Product.class, product.getRid());
		product.getParameters().add(new ProductParam(product, type, value));
		
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();	
	}


	private static void updateProductParam(int rid, ProductParamType type, Product product, String value) {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("ShopDatabase");
		EntityManager entitymanager = DatabaseManager.FACTORY.createEntityManager();
		
		try {
			entitymanager.getTransaction().begin();
			
			ProductParam param = entitymanager.find(ProductParam.class, rid);
			param.setProductParam(type);
			param.setProduct(product);
			param.setValue(value);					
			
			entitymanager.getTransaction().commit();
		} catch (Exception e) {
			entitymanager.getTransaction().rollback();
			e.printStackTrace();
		}	
		
		entitymanager.close();
		emfactory.close();
	}
}
